# !/usr/bin/env python
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qr_generator.ui'
#
# Created: Fri May 16 14:02:38 2014
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import images_rc
""" Importaciones necesarias """
import os
import shutil
import base64
from lib.qrtools  import QR

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


''' Reescribiendo la funcion para generar QR'''
def generateQr(self):
    """print self.lineName
    from qrencode import encode
    texto = "esto es una prueba"
    imagen = encode(texto)[2]
    imagen.save("qr_generated/control1.png")"""

    cedula=str(self)
    nombre=cedula
    for i in range(1,10):
        cedula=base64.b64encode(cedula)
    code = QR(data=cedula, pixel_size=10)
    code.encode()
    src = code.get_tmp_file()
    dst = '%s/qr/%s.png'%(os.getcwd(),nombre)
    shutil.copy(src, dst)
    self.codigo_qr = dst



def retranslateUi(self, MainWindows):
    MainWindows.setWindowTitle(QtGui.QApplication.translate("MainWindows", "Generador QR", None, QtGui.QApplication.UnicodeUTF8))
    self.label.setText(QtGui.QApplication.translate("MainWindows", "Por favor, Ingrese los datos para generar su código QR.", None, QtGui.QApplication.UnicodeUTF8))
    self.labelName.setText(QtGui.QApplication.translate("MainWindows", "Nombre y apellido:", None, QtGui.QApplication.UnicodeUTF8))
    self.lineName.setText(QtGui.QApplication.translate("MainWindows", "VenCERT", None, QtGui.QApplication.UnicodeUTF8))
    self.labelCompany.setText(QtGui.QApplication.translate("MainWindows", "Empresa:", None, QtGui.QApplication.UnicodeUTF8))
    self.lineCompany.setText(QtGui.QApplication.translate("MainWindows", "Suscerte", None, QtGui.QApplication.UnicodeUTF8))
    self.labelAddress.setText(QtGui.QApplication.translate("MainWindows", "Dirección:", None, QtGui.QApplication.UnicodeUTF8))
    self.lineAddress.setText(QtGui.QApplication.translate("MainWindows", "Av. Andres Bello, Edif BFC, piso 13. Caracas - Venezuela", None, QtGui.QApplication.UnicodeUTF8))
    self.labelPhone.setText(QtGui.QApplication.translate("MainWindows", "Teléfono:", None, QtGui.QApplication.UnicodeUTF8))
    self.linePhone.setText(QtGui.QApplication.translate("MainWindows", "0800 - vencert(8362378)", None, QtGui.QApplication.UnicodeUTF8))
    self.labelEmail.setText(QtGui.QApplication.translate("MainWindows", "Email:", None, QtGui.QApplication.UnicodeUTF8))
    self.lineEmail.setText(QtGui.QApplication.translate("MainWindows", "incidentes@vencerte.gob.ve", None, QtGui.QApplication.UnicodeUTF8))
    self.labelTarget.setText(QtGui.QApplication.translate("MainWindows", "Url:", None, QtGui.QApplication.UnicodeUTF8))
    self.ButtonGenerate.setText(QtGui.QApplication.translate("MainWindows", "Generar Código QR", None, QtGui.QApplication.UnicodeUTF8))
    self.pushClose.setText(QtGui.QApplication.translate("MainWindows", "Salir", None, QtGui.QApplication.UnicodeUTF8))
    self.label_2.setText(QtGui.QApplication.translate("MainWindows", "Open source - Licence: CopyLeft", None, QtGui.QApplication.UnicodeUTF8))
    self.menuBuscar.setTitle(QtGui.QApplication.translate("MainWindows", "Ver", None, QtGui.QApplication.UnicodeUTF8))
    self.menuAcerca_de.setTitle(QtGui.QApplication.translate("MainWindows", "Acerca de", None, QtGui.QApplication.UnicodeUTF8))
    self.actionSalir.setText(QtGui.QApplication.translate("MainWindows", "Buscar", None, QtGui.QApplication.UnicodeUTF8))
    self.actionCerrar_Sistema.setText(QtGui.QApplication.translate("MainWindows", "Cerrar Sistema", None, QtGui.QApplication.UnicodeUTF8))
    self.actionCerrar_Sistema.setShortcut(QtGui.QApplication.translate("MainWindows", "Ctrl+W", None, QtGui.QApplication.UnicodeUTF8))


class Ui_MainWindows(object):
    def setupUi(self, MainWindows):
        MainWindows.setObjectName(_fromUtf8("MainWindows"))
        MainWindows.resize(800, 600)
        self.centralwidget = QtGui.QWidget(MainWindows)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.graphicsView = QtGui.QGraphicsView(self.centralwidget)
        self.graphicsView.setGeometry(QtCore.QRect(-10, 0, 811, 61))
        self.graphicsView.setStyleSheet(_fromUtf8("background-color: rgb(59, 59, 59);"))
        self.graphicsView.setObjectName(_fromUtf8("graphicsView"))
        self.iconVencert = QtGui.QLabel(self.centralwidget)
        self.iconVencert.setGeometry(QtCore.QRect(661, 9, 121, 41))
        self.iconVencert.setStyleSheet(_fromUtf8("image: url(:/newPrefix/images/vencert.png);\n"
"image: url(:/newPrefix/static/images/vencert.png);"))
        self.iconVencert.setText(_fromUtf8(""))
        self.iconVencert.setObjectName(_fromUtf8("iconVencert"))
        self.graphicsView_2 = QtGui.QGraphicsView(self.centralwidget)
        self.graphicsView_2.setGeometry(QtCore.QRect(0, 60, 801, 31))
        self.graphicsView_2.setObjectName(_fromUtf8("graphicsView_2"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 70, 601, 16))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.iconSuscerte = QtGui.QLabel(self.centralwidget)
        self.iconSuscerte.setGeometry(QtCore.QRect(-80, 90, 611, 441))
        self.iconSuscerte.setStyleSheet(_fromUtf8("image: url(:/newPrefix/static/images/logo_su2.png);"))
        self.iconSuscerte.setText(_fromUtf8(""))
        self.iconSuscerte.setObjectName(_fromUtf8("iconSuscerte"))
        self.labelName = QtGui.QLabel(self.centralwidget)
        self.labelName.setGeometry(QtCore.QRect(160, 180, 131, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.labelName.setFont(font)
        self.labelName.setObjectName(_fromUtf8("labelName"))
        self.lineName = QtGui.QLineEdit(self.centralwidget)
        self.lineName.setGeometry(QtCore.QRect(310, 176, 341, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setBold(True)
        font.setWeight(75)
        self.lineName.setFont(font)
        self.lineName.setStyleSheet(_fromUtf8("color: rgb(116, 116, 116);"))
        self.lineName.setObjectName(_fromUtf8("lineName"))
        self.labelCompany = QtGui.QLabel(self.centralwidget)
        self.labelCompany.setGeometry(QtCore.QRect(220, 220, 61, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.labelCompany.setFont(font)
        self.labelCompany.setObjectName(_fromUtf8("labelCompany"))
        self.lineCompany = QtGui.QLineEdit(self.centralwidget)
        self.lineCompany.setGeometry(QtCore.QRect(310, 220, 341, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setBold(True)
        font.setWeight(75)
        self.lineCompany.setFont(font)
        self.lineCompany.setStyleSheet(_fromUtf8("color: rgb(116, 116, 116);"))
        self.lineCompany.setObjectName(_fromUtf8("lineCompany"))
        self.labelAddress = QtGui.QLabel(self.centralwidget)
        self.labelAddress.setGeometry(QtCore.QRect(210, 260, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.labelAddress.setFont(font)
        self.labelAddress.setObjectName(_fromUtf8("labelAddress"))
        self.lineAddress = QtGui.QLineEdit(self.centralwidget)
        self.lineAddress.setGeometry(QtCore.QRect(310, 260, 341, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setBold(True)
        font.setWeight(75)
        self.lineAddress.setFont(font)
        self.lineAddress.setStyleSheet(_fromUtf8("color: rgb(116, 116, 116);"))
        self.lineAddress.setObjectName(_fromUtf8("lineAddress"))
        self.labelPhone = QtGui.QLabel(self.centralwidget)
        self.labelPhone.setGeometry(QtCore.QRect(210, 300, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.labelPhone.setFont(font)
        self.labelPhone.setObjectName(_fromUtf8("labelPhone"))
        self.linePhone = QtGui.QLineEdit(self.centralwidget)
        self.linePhone.setGeometry(QtCore.QRect(310, 300, 341, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setBold(True)
        font.setWeight(75)
        self.linePhone.setFont(font)
        self.linePhone.setStyleSheet(_fromUtf8("color: rgb(116, 116, 116);"))
        self.linePhone.setObjectName(_fromUtf8("linePhone"))
        self.labelEmail = QtGui.QLabel(self.centralwidget)
        self.labelEmail.setGeometry(QtCore.QRect(230, 340, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.labelEmail.setFont(font)
        self.labelEmail.setObjectName(_fromUtf8("labelEmail"))
        self.lineEmail = QtGui.QLineEdit(self.centralwidget)
        self.lineEmail.setGeometry(QtCore.QRect(310, 340, 341, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setBold(True)
        font.setWeight(75)
        self.lineEmail.setFont(font)
        self.lineEmail.setStyleSheet(_fromUtf8("color: rgb(116, 116, 116);"))
        self.lineEmail.setObjectName(_fromUtf8("lineEmail"))
        self.labelTarget = QtGui.QLabel(self.centralwidget)
        self.labelTarget.setGeometry(QtCore.QRect(240, 380, 41, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.labelTarget.setFont(font)
        self.labelTarget.setObjectName(_fromUtf8("labelTarget"))
        self.lineEmail_2 = QtGui.QLineEdit(self.centralwidget)
        self.lineEmail_2.setGeometry(QtCore.QRect(310, 380, 341, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setBold(True)
        font.setWeight(75)
        self.lineEmail_2.setFont(font)
        self.lineEmail_2.setStyleSheet(_fromUtf8("color: rgb(116, 116, 116);"))
        self.lineEmail_2.setText(_fromUtf8(""))
        self.lineEmail_2.setObjectName(_fromUtf8("lineEmail_2"))
        self.ButtonGenerate = QtGui.QPushButton(self.centralwidget)
        self.ButtonGenerate.setGeometry(QtCore.QRect(494, 450, 151, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.ButtonGenerate.setFont(font)
        self.ButtonGenerate.setObjectName(_fromUtf8("ButtonGenerate"))
        self.pushClose = QtGui.QPushButton(self.centralwidget)
        self.pushClose.setGeometry(QtCore.QRect(390, 450, 85, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.pushClose.setFont(font)
        self.pushClose.setObjectName(_fromUtf8("pushClose"))
        self.label_2 = QtGui.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(581, 540, 191, 20))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Open Sans"))
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet(_fromUtf8("color: rgb(117, 117, 117);"))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        MainWindows.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindows)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 23))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuBuscar = QtGui.QMenu(self.menubar)
        self.menuBuscar.setObjectName(_fromUtf8("menuBuscar"))
        self.menuAcerca_de = QtGui.QMenu(self.menubar)
        self.menuAcerca_de.setObjectName(_fromUtf8("menuAcerca_de"))
        MainWindows.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindows)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindows.setStatusBar(self.statusbar)
        self.actionSalir = QtGui.QAction(MainWindows)
        self.actionSalir.setObjectName(_fromUtf8("actionSalir"))
        self.actionCerrar_Sistema = QtGui.QAction(MainWindows)
        self.actionCerrar_Sistema.setObjectName(_fromUtf8("actionCerrar_Sistema"))
        self.menuBuscar.addSeparator()
        self.menuBuscar.addAction(self.actionSalir)
        self.menuBuscar.addAction(self.actionCerrar_Sistema)
        self.menubar.addAction(self.menuBuscar.menuAction())
        self.menubar.addAction(self.menuAcerca_de.menuAction())

        retranslateUi(self, MainWindows)
        QtCore.QObject.connect(self.actionCerrar_Sistema, QtCore.SIGNAL(_fromUtf8("activated()")), MainWindows.close)
        QtCore.QObject.connect(self.pushClose, QtCore.SIGNAL(_fromUtf8("clicked()")), MainWindows.close)



        import pdb
        pdb.set_trace()


        variable = self.labelEmail.text()



        QtCore.QObject.connect(self.ButtonGenerate, QtCore.SIGNAL('clicked()'), generateQr )
        QtCore.QMetaObject.connectSlotsByName(MainWindows)



if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindows = QtGui.QMainWindow()
    ui = Ui_MainWindows()
    ui.setupUi(MainWindows)
    MainWindows.show()
    sys.exit(app.exec_())

